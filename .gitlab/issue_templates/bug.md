Bugs can be fun if we stop being mad, and start treating it like a mystery novel.  Help bring the user into the mystery as part of the solution, not a problem!

To get the **best feedback** and best chance to **solve the problem**, please ensure you have **all the following information** --> 

- what was the issue? (e.g. when I attempt to log in I receive a message stating "Email not found")
   - what did you see / what happened
   - what did you _expect_ to see?
- is it a **new issue**?
- are there **patterns in when you experience it**?
- what **device** are you using? (e.g. google pixel 5a)
- what **operating system** are you using? (e.g. android 11)
- what **app** are you using? (e.g. surveystack in chrome for android, or BFA white label pwa)
- can **you reproduce** your experience?
   - if yes, describe it clearly step by step
- can **others reproduce** this issue?
   - if not, what's different about them or their setup?
