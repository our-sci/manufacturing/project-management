**REMEMBER! Add ~"new-idea" tag to this issue to mark it as a discussion!**

This is a template... it's works for big ideas, maybe overkill for little ones, but these are all questions worth reviewing:

### What's the idea? 
>_lay it on us!_

### What problems does it solve and what opportunities does it open?

### What's the short / long term impact if successful, and what's the risks if not?
> _we perfer low risk high reward :)  If not, how can you adjust the idea to mitigate risks, or find additional rewards you didn't originally consider?_

### How does it relate to your goals, our partners goals, our current projects/priorities or our mission
> _if it doesn't hit one of these at least a little, you should seriously consider taking it somewhere that's a better fit_

### How much effort (hours, and timeframe) will it take and who's motivated to actually get it done?
> _even perfect ideas need motivated drivers... who is it?  If you think you know, go ask them and get their buy in_

### What will it cost, and if it costs something... how's it funded?
> _if there's not funding, go find some sources to bring to the discussion.  If there is funding - great work, now figure out how to leverage it to derisk or fund other stuff at the same time (5 birds, 1 stone)!_

### Who have you talked to about it... what do they think?
> _big ideas need lots of discussion - to really refine it talk to everyone you can!  Seriously, if it's a big idea you should talk to at least 4 people about this and consider / integrate their feedback_

Refresher: [How we do project management link](https://gitlab.com/groups/our-sci/-/wikis/Getting-Started)
